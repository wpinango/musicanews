import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class NetworkScreen extends StatefulWidget {
  final url;
  final name;
  const NetworkScreen({Key key, this.url, this.name}) : super(key: key);

  @override
  NetworkScreenState createState() => new NetworkScreenState(url: url, name: name);
}

class NetworkScreenState extends State<NetworkScreen> {
  InAppWebViewController webView;
  BuildContext context;
  double progress = 0;
  String url;
  String name;

  NetworkScreenState({this.url, this.name});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text(name),

        ),
        body: Container(
            //margin: EdgeInsets.only(top: 24),
            child: Column(children: <Widget>[
          Container(
              child: progress < 1.0
                  ? LinearProgressIndicator(value: progress)
                  : Container()),
          Expanded(
            child: Container(
              //decoration:
                  //BoxDecoration(border: Border.all(color: Colors.blueAccent)),
              child: InAppWebView(
                initialUrl: url,
                initialHeaders: {},
                initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                  debuggingEnabled: true,
                )),
                onWebViewCreated: (InAppWebViewController controller) {
                  webView = controller;
                },
                onLoadStart: (InAppWebViewController controller, String url) {
                  setState(() {
                  });
                },
                onLoadStop:
                    (InAppWebViewController controller, String url) async {
                  setState(() {
                  });
                },
                onProgressChanged:
                    (InAppWebViewController controller, int progress) {
                  setState(() {
                    this.progress = progress / 100;
                  });
                },
              ),
            ),
          ),
        ])));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:musicanews/screens/news_screen.dart';
import 'package:splashscreen/splashscreen.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key key}) : super(key: key);

  @override
  FirstScreenState createState() => new FirstScreenState();
}

class FirstScreenState extends State<FirstScreen> {
  bool isLogin = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 4,
      navigateAfterSeconds: NewsScreen(),
      imageBackground: AssetImage("assets/screen.jpg"),
      styleTextUnderTheLoader: new TextStyle(color: Colors.white),
      loaderColor: Colors.white,
    );
  }
}
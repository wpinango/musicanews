import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:musicanews/screens/network_webview_screen.dart';
import 'package:musicanews/widgets/panel.dart';
import 'package:musicanews/widgets/transparent_button.dart';

import '../global.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key key}) : super(key: key);

  @override
  NewsScreenState createState() => new NewsScreenState();
}

class NewsScreenState extends State<NewsScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  InAppWebViewController webView;
  double progress = 0;
  BorderRadius border = BorderRadius.only(
      topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0));

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.black,
        body: SlidingUpPanel(
          backdropEnabled: true,
          panelSnapping: true,
          minHeight: size.height / 10,
          maxHeight: size.height / 1.95,
          panel: Container(
            decoration: BoxDecoration(
              borderRadius: border,
              image: DecorationImage(
                  image: AssetImage('assets/public2.png'), fit: BoxFit.cover),
            ),
            child: Container(
              margin: EdgeInsets.all(48),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/instagram.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.igUrl, 'Instagram');
                        },
                      ),
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/facebook.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.fbUrl, 'Facebook');
                        },
                      ),
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/twitter.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.ttUrl, 'Twitter');
                        },
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/flipboard.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.fpUrl, 'Flipboard');
                        },
                      ),
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/tumblr.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.tmUrl, 'Tumblr');
                        },
                      ),
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/pinterest.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.ptUrl, 'Pinterest');
                        },
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Image(
                          image: AssetImage('assets/linkedin.png'),
                        ),
                        onPressed: () {
                          openWebViewScreen(Global.lkUrl, 'Linkedin');
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          collapsed: Container(
            decoration: BoxDecoration(
              //image: DecorationImage(image: AssetImage('assets/feature.jpg'), fit: BoxFit.fitWidth),
              borderRadius: border,
            ),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.5), borderRadius: border),
                child: Center(
                  child: Text(
                    "Redes Sociales",
                    style: GoogleFonts.varelaRound(
                        textStyle:
                            TextStyle(color: Colors.white, fontSize: 20)),
                  ),
                ),
              ),
            ),
          ),
          body: Container(
            margin: EdgeInsets.only(top: 24),
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/screen.jpg'), fit: BoxFit.cover)),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(36),
                ),
                TransparentButton(
                  buttonColor: Colors.white.withOpacity(0.4),
                  buttonName: 'ENTRA',
                  textStyle: GoogleFonts.varelaRound(
                      textStyle: TextStyle(color: Colors.white)),
                  width: size.width / 2,
                  onTap: () {
                    _launchURL(context);
                  },
                ),
              ],
            ),
          ),
          borderRadius: border,
        ));
  }

  void _launchURL(BuildContext context) async {
    try {
      await launch(
        Global.mainUrl,
        option: new CustomTabsOption(
          toolbarColor: Theme.of(context).primaryColor,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      debugPrint(e.toString());
      showInSnackBar('Ha ocurrido un problema al abrir la pagina');
    }
  }

  openWebViewScreen(String url, String name) async {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new NetworkScreen(
                  url: url,
                  name: name,
                )));
  }
}

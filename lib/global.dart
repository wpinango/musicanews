class Global {
  static String mainUrl = 'https://musica.news/';
  static String fbUrl = 'https://www.facebook.com/MusicaNewsOfficial/';
  static String ttUrl = 'https://twitter.com/musicanews_tw';
  static String igUrl = 'https://www.instagram.com/musicanews_in/';
  static String lkUrl = 'https://www.linkedin.com/company/m%C3%BAsica-news/';
  static String fpUrl = 'https://flipboard.com/@MusicaNews';
  static String tmUrl = 'https://musicanews.tumblr.com/';
  static String ptUrl = 'https://www.pinterest.com/musicanews_pin/';
}
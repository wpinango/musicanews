import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TransparentButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onTap;

  final double height;
  final double width;
  final double bottomMargin;
  final double borderWidth;
  final Color buttonColor;
  final TextStyle textStyle;

  TransparentButton(
      {this.buttonName,
        this.onTap,
        this.height,
        this.bottomMargin,
        this.borderWidth,
        this.width,
        this.textStyle,
        this.buttonColor});

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: width,
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
        child: Container(
          width: width,
          color: buttonColor,
          child: new FlatButton(
              child: new Text(
                buttonName,
                style: GoogleFonts.varelaRound(textStyle: textStyle),
              ),
              onPressed: onTap
          ),
        ),
      ),
    );
  }
}
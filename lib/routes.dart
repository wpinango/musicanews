import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musicanews/screens/news_screen.dart';
import 'package:musicanews/screens/splash_screen.dart';

class Routes {

  var routes = <String, WidgetBuilder> {
    '/news' : (BuildContext context) => new NewsScreen(),
  };

  Routes() {
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Musica News",
      home: new FirstScreen(),
      routes: routes,
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
    ));
  }

  static replacementScreen(BuildContext context, String routeName) {
    Navigator.pushReplacementNamed(context, routeName);
  }

  static showModalScreen(BuildContext context, String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }
}